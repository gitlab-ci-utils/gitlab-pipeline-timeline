import globals from 'globals';
import html from 'eslint-plugin-html';
import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended-esm.js';

export default [
  ...recommendedConfig,
  {
    languageOptions: {
      globals: {
        ...globals.browser
      },
      sourceType: 'module'
    },
    plugins: { html },
    rules: {
      'node/no-unsupported-features/es-syntax': 'off',
      'node/no-unsupported-features/node-builtins': 'off'
    },
    settings: {
      'html/html-extensions': ['.html']
    }
  },
  {
    files: ['./src/index.html'],
    languageOptions: {
      globals: {
        google: 'readonly'
      }
    }
  },
  {
    ignores: ['.vscode/**', 'Archive/**', 'node_modules/**', 'coverage/**'],
    name: 'ignores'
  }
];
