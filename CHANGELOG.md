# Changelog

## v0.4.0 (2023-11-13)

### Changed

- Updated timeline to show retried pipeline jobs. Thanks @Oleksandr.Grechanov.
  (#20)
- Updated `node:engines` to v22, now the Active LTS release as of 2024-10-29.
  (#18)

## v0.3.1 (2023-11-12)

### Fixed

- Fixed issue rendering pipeline graph with incomplete jobs, now showing only
  `success` and `failed` jobs. Thanks @Oleksandr.Grechanov. (#19)
- Updated to `serve@14.2.4`, resolving CVE-2024-45296.

## v0.3.0 (2023-10-12)

### Changed

- Updated CSS to use logical properties.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#15)
- Updated `engine` to Node 20. (#14)

## v0.2.1 (2023-04-29)

### Fixed

- Fix project and pipeline links to point to host based on the URL. (#11)

## v0.2.0 (2023-04-17)

### Changed

- Updated page to pull GitLab URL from the pipeline URL to allow use on
  private GitLab instances.
- Added capability to provide acces token to view pipeline timelines for
  non-public projects. See details and warning in the README.

## v0.1.0 (2022-10-10)

Initial release
