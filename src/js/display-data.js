/** @module Display-Data */

import {
    getBasePipelineUrl,
    getPipelineData,
    getPipelineDataFromUrl
} from './gitlab.js';

/**
 * @typedef  {object} PipelineData
 * @property {string} projectName  The full name of the project (namespace/project).
 * @property {number} pipelineId   The ID of the project pipeline.
 * @private
 */

/**
 * Displays the project name and pipeline ID, with links, on the page.
 *
 * @param {PipelineData} pipelineData The project pipeline data.
 * @static
 * @private
 */
const displayPipelineData = ({ projectName, pipelineId }) => {
    const projectNameElement = document.querySelector('#chart-title');
    const { hostname } = getBasePipelineUrl();

    if (projectNameElement) {
        // Dynamically adding HTML to the page based on submitted URL.
        // nosemgrep: insecure-document-method, insecure-innerhtml, html-in-template-string
        projectNameElement.innerHTML = `Project: <a href="https://${hostname}/${projectName}/" target="_blank">${projectName}</a>`;
    }

    const pipelineIdElement = document.querySelector('#pipeline-id');
    if (pipelineIdElement) {
        // Dynamically adding HTML to the page based on submitted URL.
        // nosemgrep: insecure-document-method, insecure-innerhtml, html-in-template-string
        pipelineIdElement.innerHTML = `Pipeline: <a href="https://${hostname}/${projectName}/-/pipelines/${pipelineId}" target="_blank">${pipelineId}</a>`;
    }
};

/**
 * Gets the timeline chart data table definition.
 *
 * @param   {object} google Google chart object.
 * @returns {object}        The timeline chart data table.
 * @static
 * @private
 */
const getChartDataTable = (google) => {
    const data = new google.visualization.DataTable();
    data.addColumn({ id: 'Job', type: 'string' });
    data.addColumn({ id: 'Start', type: 'date' });
    data.addColumn({ id: 'End', type: 'date' });
    return data;
};

/**
 * Get the timeline chart options based on number of rows.
 *
 * @param   {number} rowCount The number of rows in teh timeline.
 * @returns {object}          The timeline chart options.
 * @static
 * @private
 */
const getChartOptions = (rowCount) => {
    const trackHeight = 41;
    const chartMarginTracks = 2;
    return {
        hAxis: { format: 'mm:ss' },
        height: trackHeight * (rowCount + chartMarginTracks),
        timeline: {
            rowLabelStyle: { fontSize: 14 }
        },
        width: '100%'
    };
};

/**
 * Retrieves pipeline data and generates chart.
 *
 * @param {object} google Google chart object.
 * @async
 * @static
 * @public
 */
async function displayChart(google) {
    const { projectName, pipelineId } = getPipelineDataFromUrl();

    const data = getChartDataTable(google);
    const pipelineData = await getPipelineData({
        pipelineId,
        projectName
    });
    data.addRows(pipelineData);

    displayPipelineData({ pipelineId, projectName });
    const chartContainer = document.querySelector('#chart-div');
    const chart = new google.visualization.Timeline(chartContainer);
    chart.draw(data, getChartOptions(pipelineData.length));
}

export { displayChart };
