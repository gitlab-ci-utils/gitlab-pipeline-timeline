# GitLab Pipeline Timeline

An MVC of a utility to visualize the actual timeline of GitLab CI jobs in a completed pipeline. This can be used to determine unnecessary dependencies, and potential improvements from leveraging the new `medium` and `large` gitlab.com Linux container runners.

**_Note: this utility uses [Google Charts](https://developers.google.com/chart/interactive/docs/gallery/timeline) to generate the timeline and in opening the page your browser makes a request to Google for their script._**

## Usage

Go to the pages site for this project at https://gitlab-ci-utils.gitlab.io/gitlab-pipeline-timeline/, enter the URL for a pipeline, and push the `Get Timeline` button. The GitLab URL, namespace, project, and pipeline ID are taken from the URL and the actual timeline of the pipeline is displayed. The timeline chart includes all jobs, and triggered pipelines (displayed as one job), ordered based on the actual start time, as shown below. All times are rounded to the nearest second.

![timeline screenshot](./assets/timeline-screenshot.png)

Hovering over any job's bar displays a popup with the job's start/end time relative to the start of the pipeline and the duration of the job.

### Timeline links

To create a link to a specific pipeline's timeline, the pipeline URL can be provide via the `pipelineUrl` query string parameter.

### Non-public pipeline timelines

Non-public pipelines on any accessible GitLab instance can be viewed if an access token is provided (personal, project, or group access token). This could be for private or internal projects, or public projects where the CI/CD permissions to not allow public access. The token must be added to the page's local storage key `gitlab-pipeline-timeline`. For example, from the developer tools console run:

```js
window.localStorage.setItem('gitlab-pipeline-timeline', '<access token>');
```

> Note: unencrypted data in `localstorage` is accessible to any JavaScript running on the page, including browser extensions, as well as any other page according to the [same-origin policy](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy). This could compromise the access token, as noted [here](https://cheatsheetseries.owasp.org/cheatsheets/HTML5_Security_Cheat_Sheet.html#local-storage) and [here](https://auth0.com/docs/secure/security-guidance/data-security/token-storage#browser-local-storage-scenarios). If this risk is acceptable, using a token with the minimum required permissions is recommended. For example, for projects with [Public Pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#change-which-users-can-view-your-pipelines), a [Project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with Guest role and read_api scope is sufficient. If Public Pipelines is disabled, the Reporter role and read_api scope is required.

## Limitations

This in initial MVC, and as such has a number of limitations:

- Only the first 100 jobs and 100 triggered pipelines (that is, pipeline bridges) are displayed. If the pipeline has more than this, any others are not displayed, and the pipeline duration only reflects the displayed jobs.
- If a job is retried only the last execution is displayed.
- For short duration jobs where the minute of the start and end times are the same, the popup only shows the seconds (for example if the jobs runs from 3:30 - 3:53, the popup shows "30s - 53s" and "Duration: 23 seconds").
- Attempting to display timelines for pipelines that aren't complete has shown inconsistent results, and may generate an error and not display the timeline.
- Most errors are written only to the console.

The following limitations exist if an access token isn't provided:

- Only timelines for public projects can be viewed (on any accessible GitLab instance).
- Only timelines for publicly visible project pipelines can be viewed. This requires:
  - The project visibility must be set to "Public" (in General settings).
  - The project CI/CD permissions must be set to "Everyone With Access" (in General settings).
  - The "Public Pipelines" property must be enabled (in CI/CD settings).
